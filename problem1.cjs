
const fs = require(`fs`);

const path = require(`path`);

function makeDirectory(filepath) {

    return new Promise((resolve, reject) => {

        fs.access(filepath, err => {

            if (err) {
                fs.mkdir(filepath, (err) => {

                    if (err) {
                        reject(err);

                    } else {

                        resolve("New Directory is created");

                    }
                })
            } else {
                resolve("Directory is present");
            }
        });

    });

}


function createJSONFiles(fileLocation, num = 10) {

    let createdFiles = [];

    let errors = [];

    let executedCount = 0;

    return new Promise((resolve, reject) => {


        for (let index = 0; index < num; index++) {

            let randomName = new Date().getTime().toString() + Math.random().toFixed(2);

            let fileName = randomName + ".Json"

            fs.writeFile(path.join(fileLocation, fileName), "HelloWorld" + randomName, (err) => {

                if (err) {
                    errors.push(err);
                } else {
                    createdFiles.push(fileName);
                }

                executedCount++;

                if (executedCount === num) {


                    if (createdFiles.length === num) {

                        resolve(createdFiles);

                    } else {

                        resolve("All files were not created sucessfully!!");

                    }

                }


            });


        }


    })

}


function deleteJSONFiles(fileLocation, filesArray) {

    return new Promise((resolve, reject) => {

        let deletedFiles = [];

        let errors = [];

        let executedCount = 0;

        filesArray.forEach(element => {

            fs.unlink(path.join(fileLocation, element), (err) => {

                if (err) {

                    errors.push(err);
                } else {

                    deletedFiles.push(element);

                }


                executedCount++;

                if (executedCount === filesArray.length) {


                    if (deletedFiles.length === filesArray.length) {

                        resolve("All files Deleted Sucessfully!!");
                    } else {

                        reject("All files were not Deleted sucessfully!!");

                    }

                }

            });


        });


    });

};


module.exports.makeDir = makeDirectory;

module.exports.createFiles = createJSONFiles;

module.exports.deleteFiles = deleteJSONFiles;