
const problem2Function = require(`../problem2.cjs`);

const fileName = "lipsum.txt";


const fsAsyncProblem2 = async () => {

    try {

        let data1 = await problem2Function.read(fileName);


        let data2 = await problem2Function.toUpperCase(data1);



        let data3 = await problem2Function.TolowercaseSplit(data2);


        let alldata = await problem2Function.Sort(data3);


        let dl = await problem2Function.delete();




        console.log(dl);

    }
    catch (err) {

        console.error(err);
    }

};

fsAsyncProblem2();