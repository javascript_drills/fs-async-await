const problem1 = require(`../problem1.cjs`);

const fileLocation = "../jsonFiles";

const numberOfFiles = 5;

const fsAsync = async () => {

    try {

        const respond = await problem1.makeDir(fileLocation);

        console.log(respond);

        const rep = await problem1.createFiles(fileLocation, numberOfFiles);

        console.log("files created");

        const deleteFiles = await problem1.deleteFiles(fileLocation, rep);

        console.log(deleteFiles);

    }
    catch (error) {
        console.log(error);
    }

};


fsAsync();